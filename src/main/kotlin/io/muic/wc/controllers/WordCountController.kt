package io.muic.wc.controllers

import io.muic.wc.dto.BaseDTO
import io.muic.wc.dto.ErrorDTO
import io.muic.wc.dto.WordCountDTO
import io.muic.wc.services.TextService
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.jsoup.HttpStatusException
import org.jsoup.UnsupportedMimeTypeException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.CacheConfig
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitter
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.Callable

@Controller
class WordCountController {

    companion object {
        val logger: Logger = LogManager.getLogger(WordCountController::class.java)
    }

    @Autowired
    private lateinit var textService: TextService

    private fun getResult(url: String, force: Boolean?): BaseDTO {
       return try {
           when(force) {
                true -> textService.getResultForced(url)
                else -> textService.getResult(url)
            }
        } catch (mue: IllegalArgumentException) {
            ErrorDTO(HttpStatus.BAD_REQUEST.value(), "The request URL is unknown, not a HTTP or HTTPS URL, or is otherwise malformed.")
        } catch (uhe: UnknownHostException) {
            ErrorDTO(HttpStatus.BAD_REQUEST.value(), "Unable to resolve host.")
        } catch (hse: HttpStatusException) {
            ErrorDTO(HttpStatus.NOT_ACCEPTABLE.value(), "The response from the target is not OK and HTTP response errors are not ignored.")
        } catch (umte: UnsupportedMimeTypeException) {
            ErrorDTO(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value(), "The response mime type from the target is not supported and those errors are not ignored.")
        } catch (ste: SocketTimeoutException) {
            ErrorDTO(HttpStatus.REQUEST_TIMEOUT.value(), "The connection times out.")
        } catch (e: IOException) {
            ErrorDTO(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error occurs.")
        }
}

@GetMapping(value = ["/wc"], headers = ["Accept=application/json"])
    fun jsonWc(@RequestParam("target") url: String, @RequestHeader("Accept") accept: String, @RequestHeader("force") force: Boolean?): Callable<ResponseEntity<out BaseDTO>> {
        val result: BaseDTO = getResult(url, force)
        return Callable {
            when (result) {
                is WordCountDTO -> ResponseEntity(result, HttpStatus.OK)
                else -> {
                    val error: ErrorDTO = result as ErrorDTO
                    ResponseEntity(error, HttpStatus.valueOf(error.code))
                }
            }
        }
    }

    @GetMapping(value = ["/wc"], headers = ["Accept=text/plain"])
    fun plainWc(@RequestParam("target") url: String, @RequestHeader("Accept") accept: String, @RequestHeader("force") force: Boolean?): Callable<ResponseEntity<String>> {
        val result: BaseDTO = getResult(url, force)
        return Callable {
            when (result) {
                is WordCountDTO -> ResponseEntity("Total Count: ${result.count} \nTop 10: ${result.top}", HttpStatus.OK)
                else -> {
                    val error: ErrorDTO = result as ErrorDTO
                    ResponseEntity("Error ${error.code} \n${error.message}", HttpStatus.valueOf(error.code))
                }
            }
        }
    }

    @GetMapping(value = ["/wc"])
    fun htmlWc(model: Model, @RequestParam("target") url: String, @RequestHeader("Accept") accept: String, @RequestHeader("force") force: Boolean?): Callable<String> {
        val result: BaseDTO = getResult(url, force)
        return Callable {
            when (result) {
                is WordCountDTO -> {
                    model.addAttribute("name", url)
                    model.addAttribute("count", result.count)
                    model.addAttribute("tops", result.top)
                    "wc"
                }
                else -> {
                    val error: ErrorDTO = result as ErrorDTO
                    model.addAttribute("code", error.code)
                    model.addAttribute("message", error.message)
                    "error"
                }
            }
        }
    }
}