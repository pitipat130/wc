package io.muic.wc.controllers

import io.muic.wc.services.TextService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class CacheController {

    @Autowired
    private lateinit var textService: TextService

    @DeleteMapping("/wc")
    fun clearCache() {
        textService.clearResults()
    }
}