package io.muic.wc

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cache.annotation.EnableCaching

@SpringBootApplication
class WcApplication

fun main(args: Array<String>) {
    runApplication<WcApplication>(*args)
}
