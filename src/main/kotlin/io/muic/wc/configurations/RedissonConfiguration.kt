package io.muic.wc.configurations

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.io.IOException
import org.springframework.cache.CacheManager
import org.redisson.spring.cache.RedissonSpringCacheManager
import java.util.HashMap
import org.redisson.api.RedissonClient
import org.redisson.Redisson
import org.redisson.config.Config
import org.redisson.spring.cache.CacheConfig
import org.springframework.cache.annotation.EnableCaching


@Configuration
@EnableCaching
class RedissonConfiguration {

    @Value("\${spring.redis.host}")
    private lateinit var redisHost: String
    @Value("\${spring.redis.port}")
    private val redisPort: Int = 6379

    @Bean(destroyMethod = "shutdown")
    @Throws(IOException::class)
    fun redisson(): RedissonClient {
        val config = Config()
        config.useSingleServer()
                .setAddress("redis://$redisHost:$redisPort")
        return Redisson.create(config)
    }

    @Bean
    fun cacheManager(redissonClient: RedissonClient): CacheManager {
        val config = HashMap<String, CacheConfig>()

        val options = CacheConfig(2 * 60 * 1000, 2 * 60 * 1000)
        options.maxSize = 1000
        config["urls"] = options
        return RedissonSpringCacheManager(redissonClient, config)
    }
}