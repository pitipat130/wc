package io.muic.wc.services

import io.muic.wc.dto.BaseDTO
import io.muic.wc.dto.ErrorDTO
import io.muic.wc.dto.WordCountDTO
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.jsoup.Connection
import org.jsoup.HttpStatusException
import org.jsoup.Jsoup
import org.jsoup.UnsupportedMimeTypeException
import org.redisson.api.RMap
import org.redisson.api.RMapCache
import org.redisson.api.RedissonClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.CachePut
import org.springframework.cache.annotation.Cacheable
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import java.io.IOException
import java.lang.IllegalArgumentException
import java.net.MalformedURLException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.math.log

@Service
class TextService {

    @Autowired
    private lateinit var client: RedissonClient

    companion object {
        val logger: Logger = LogManager.getLogger(TextService::class.java)
    }

    @Cacheable(value = ["urls"], key = "#url", sync = true)
    @Throws(IllegalArgumentException::class,
            UnknownHostException::class,
            HttpStatusException::class,
            UnsupportedMimeTypeException::class,
            SocketTimeoutException::class,
            IOException::class
    )
    fun getResult(url: String): BaseDTO {
        return getResultForced(url)
    }

    @CachePut(value = ["urls"], key = "#url", unless = "#result.error != true ")
    @Throws(IllegalArgumentException::class,
            UnknownHostException::class,
            HttpStatusException::class,
            UnsupportedMimeTypeException::class,
            SocketTimeoutException::class,
            IOException::class
    )
    fun getResultForced(url: String): BaseDTO {
        logger.info("Cache Miss")
        return fetchResult(url)
    }

    @CacheEvict(value = ["urls"], key = "#url")
    fun removeResult(url: String) {}

    @CacheEvict(value = ["urls"], allEntries = true)
    fun clearResults() {}

    @Throws(IllegalArgumentException::class,
            UnknownHostException::class,
             HttpStatusException::class,
             UnsupportedMimeTypeException::class,
             SocketTimeoutException::class,
             IOException::class
    )
    private fun fetchResult(url: String): BaseDTO {
        val map: RMapCache<String, Pair<String, String>> = client.getMapCache("etags")
        val resp : Connection.Response = if (map.containsKey(url)) {
            Jsoup.connect(url).header("If-None-Match", map[url]!!.first).execute()
        } else {
            Jsoup.connect(url).execute()
        }
        return if (map.containsKey(url) && resp.statusCode() == 304) {
            countWords(map[url]!!.second)
        } else {
            val doc: String = resp.parse().body().text()
            if (resp.header("ETag") != null) {
                map.put(url, Pair(resp.header("ETag"), doc), 5, TimeUnit.MINUTES)
            }
            countWords(doc)
        }
}

private fun countWords(text: String): WordCountDTO {
        val words: MutableMap<String, Int> = mutableMapOf()
        val matcher: Matcher = Pattern.compile("[a-zA-Z]{1,20}")
                .matcher(text)
        var count: Int = 0
        while (matcher.find()) {
            val word: String = matcher.group().toLowerCase()
            count += 1
            when (words.containsKey(word)) {
                true -> words[word] = words[word]!! + 1
                false -> words[word] = 1
            }
        }
        val top: List<String> = words
                                    .toList()
                                    .sortedBy { it.second }
                                    .reversed()
                                    .map { it.first }
                                    .take(10)
        return WordCountDTO(count, top)
    }

}