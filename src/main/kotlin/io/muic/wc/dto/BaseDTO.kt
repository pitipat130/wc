package io.muic.wc.dto

import com.fasterxml.jackson.annotation.JsonIgnore
import java.io.Serializable

abstract class BaseDTO (
    @JsonIgnore
    val error: Boolean = false
) : Serializable {
    companion object {
        private const val serialVersionUID : Long = 274587897016077468L
    }
}
