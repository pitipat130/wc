package io.muic.wc.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class WordCountDTO(
        @JsonProperty("total_count")
        val count: Int,
        @JsonProperty("top10")
        val top: List<String>
) : BaseDTO() {
        companion object {
                private const val serialVersionUID : Long = 274587897016077468L
        }
}