package io.muic.wc.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class ErrorDTO (
        @JsonProperty("status_code")
        val code: Int,
        @JsonProperty("message")
        val message: String
) : BaseDTO(true) {
        companion object {
                private const val serialVersionUID : Long = 274587897016077468L
        }
}